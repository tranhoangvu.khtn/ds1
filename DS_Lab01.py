# -*- coding: utf-8 -*-
"""
Created on Fri Jun 21 21:05:08 2019

@author: tranh
"""


from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from IPython.display import display

from sklearn.tree import DecisionTreeRegressor

from sklearn import metrics
import pandas as pd
import numpy as np
from structured import *
from Lib import *
from FunctionRule import *
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.linear_model import LinearRegression
from sklearn.externals import joblib

def main(): 
        
    df_raw = pd.read_csv(f'D:\Data\BlueBook\Train.csv', low_memory=False, 
                         parse_dates=["saledate"])
    df_raw.shape
    
    
    
    display_all(df_raw.tail().T)
    
    display_all(df_raw.describe(include='all').T)
    
    
    df_raw.SalePrice = np.log(df_raw.SalePrice)
    
    tree = DecisionTreeRegressor(max_depth=3)
    tree.fit(df_raw.drop('SalePrice', axis=1), df_raw.SalePrice)
    
    
    #add_datepart(df_raw, 'saledate')
    df_raw.saleYear.head()
    
    display_all(df_raw.describe(include='all').T)
    
    train_cats(df_raw)
    
    df_raw.UsageBand.cat.categories
    
    
    df_raw.UsageBand.cat.set_categories(['High', 'Medium', 'Low'], ordered=True, inplace=True)
    df_raw.UsageBand = df_raw.UsageBand.cat.codes
    
    print(df_raw.UsageBand)
    
    display_all(df_raw.isnull().sum().sort_index()/len(df_raw))
    
    df, y, nas = proc_df(df_raw, 'SalePrice')
    
    df.head()
    
    # Note: change max_depth
    tree = DecisionTreeRegressor(max_depth=15)
    tree.fit(df, y)
    tree.score(df,y)
    
    #Data Pre-processing
    
    n_valid = 120000  # same as Kaggle's test set size
    n_trn = len(df)-n_valid
    raw_train, raw_valid = split_vals(df_raw, n_trn)
    X_train, X_valid = split_vals(df, n_trn)
    y_train, y_valid = split_vals(y, n_trn)
    
    X_train.shape, y_train.shape, X_valid.shape, y_valid.shape
    print('-------------------------------------------------------')
    print('----------------------Model---------------------------------')
    
    
    tree = DecisionTreeRegressor(max_depth=3)
    %time tree.fit(X_train, y_train)
    print_score(tree)
    
    
    print('-------------------------------------------------------')
    print('-------------------------------------------------------')
    
    df_trn, y_trn, nas = proc_df(df_raw, 'SalePrice', subset=30000, 
                         na_dict=nas)
    X_train, _ = split_vals(df_trn, 20000)
    y_train, _ = split_vals(y_trn, 20000)
    
    tree = DecisionTreeRegressor(max_depth=3)
    %time tree.fit(X_train, y_train)
    print_score(tree)
    
    #draw_tree(tree, df_trn)
    
    print('------------------Another way to split data------------')
    print('-------------------------------------------------------')
    X_train, X_test, y_train, y_test = train_test_split(
                df,
                y,
                test_size=0.25,
                random_state=42,
    )

    
    tree = DecisionTreeRegressor(max_depth=5)
    %time tree.fit(X_train, y_train)
    tree.score(X_train, y_train)
    tree.score(X_test, y_test)
    
    
    print('------------------Linear Regression--------------------')
    print('-------------------------------------------------------')
    
    
    df_raw['age'] = df_raw.saleYear-df_raw.YearMade
    
    df, y, nas, mapper = proc_df(df_raw, 'SalePrice', max_n_cat=10, do_scale=True)
    
    n_valid = 12000
    n_trn = len(df)-n_valid
    y_train, y_valid = split_vals(y, n_trn)
    raw_train, raw_valid = split_vals(df_raw, n_trn)
    
    df.describe().transpose()
    X_train, X_valid = split_vals(df, n_trn)
    
    m = LinearRegression().fit(X_train, y_train)
    m.score(X_valid, y_valid)
    
    m.score(X_train, y_train)
    
    preds = m.predict(X_valid)
    
    rmse(preds, y_valid)
    
    plt.scatter(preds, y_valid, alpha=0.1, s=2);
    
    print('------------------Save the trained model to a file-----')
    print('-------------------------------------------------------')
    
    
    filename = 'finalized_model.sav'
    joblib.dump(tree, filename)    
    
    loaded_model = joblib.load(filename)
    result = loaded_model.score(X_test, y_test)
    print(result)
     
    print("Results Using RandomForestRegressor:") 
    #m = RandomForestRegressor(n_jobs=-1)
    m = RandomForestRegressor(n_estimators=1, max_depth=5, bootstrap=False, n_jobs=-1)
    m.fit(X_train, y_train)
    print_score(m)
    
    
    m = RandomForestRegressor(n_estimators=40, min_samples_leaf=3, max_features=0.5, n_jobs=-1, oob_score=True) 
    m.fit(X_train, y_train)
    print_score(m)
    
        
# Calling main function 
if __name__=="__main__": 
    main() 